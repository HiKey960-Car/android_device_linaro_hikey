#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <pthread.h>
#include <cutils/properties.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <sys/socket.h>
#include <sys/un.h>

#include <hardware/lights.h>

#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <dirent.h>

#include <sys/stat.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <android/log.h>

#define MAX_PATH_SIZE 80

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;

int hid_fd = -1;
unsigned char lastHID = 0x20;
static uint8_t minBright = 0x0;

void init_globals(void){
    pthread_mutex_init(&g_lock, NULL);
}

void writehid(uint8_t cmd, uint8_t value, uint8_t len){
    uint8_t buf[16];
    int res;

    buf[0] = 0x0;   // Report ID (0)
    buf[1] = cmd;   // Command (0x20 - query, 0x03 - set BL)
    buf[2] = value; // Value to set

    res = write(hid_fd, buf, len+1);
    if (res < 0){
        __android_log_print(ANDROID_LOG_DEBUG, "lights", "HID write error: %d", errno);
        if (errno != 75 && errno != 71 && errno != 32){
            close(hid_fd);
            hid_fd = -1;
        }
    }
}

int hidrawdev(const struct dirent *d){
    if (strstr(d->d_name, "hidraw") != NULL) return 1;
    return 0;
}

void openhid(){
    int n, res;
    struct dirent **filelist;
    char buf[256];
    char path[16];
    struct hidraw_report_descriptor rpt_desc;
    struct hidraw_devinfo info;

    n = scandir("/dev", &filelist, *hidrawdev, NULL);
    __android_log_print(ANDROID_LOG_DEBUG, "lights", "Found %d hidraw devices.", n);
    while (n--){
        snprintf(path, sizeof(path), "/dev/%s", filelist[n]->d_name);
        free(filelist[n]);
        __android_log_print(ANDROID_LOG_DEBUG, "lights", "File: %s", path);

        if (hid_fd < 0){
            hid_fd = open(path, O_RDWR|O_NONBLOCK);
            if (hid_fd < 0) continue;
            __android_log_print(ANDROID_LOG_DEBUG, "lights", "file opened");

            memset(&rpt_desc, 0x0, sizeof(rpt_desc));
            memset(&info, 0x0, sizeof(info));
            memset(buf, 0x0, sizeof(buf));

            /* Get Raw Info */
            res = ioctl(hid_fd, HIDIOCGRAWINFO, &info);

            if ((int16_t)info.vendor != (int16_t)0x239a){
                __android_log_print(ANDROID_LOG_DEBUG, "lights", "wrong vendor");
                close(hid_fd);
                hid_fd = -1;
                continue;
            }

            if ((int16_t)info.product != (int16_t)0x801e){
                __android_log_print(ANDROID_LOG_DEBUG, "lights", "wrong product");
                close(hid_fd);
                hid_fd = -1;
                continue;
            }
            __android_log_print(ANDROID_LOG_DEBUG, "lights", "device OK, checking endpoint");

            // Command 0x20 requests which type, 0x02 = backlight/fan
            writehid(0x20, 0x00, 1);

            // Try to read data from this endpoint
            for (int i=0; i<10; i++){
                if ((res = read(hid_fd, buf, 16)) > 0){
                    __android_log_print(ANDROID_LOG_DEBUG, "lights", "received data: %02X\n", buf[0]);
                    // If we read 0x03, leave the fd open, further iterations of the outer
                    // loop are just going to free the remaining dirent's.
                    if (buf[0] != 0x02){
                        close(hid_fd);
                        hid_fd = -1;
                    }
                    break;
                }
                usleep(10000);
            }
            if (res <= 0 && hid_fd >= 0){
                close(hid_fd);
                hid_fd = -1;
            }
        }
    }
    free(filelist);
}

static int rgb_to_brightness(struct light_state_t const* state){
    int color = state->color & 0x00ffffff;
    return ((77*((color>>16)&0x00ff))
            + (150*((color>>8)&0x00ff)) + (29*(color&0x00ff))) >> 8;
}

static int set_light_backlight(struct light_device_t* dev,
        struct light_state_t const* state){
    int brightness = rgb_to_brightness(state);
    unsigned char new_brightness = (unsigned char)brightness;

    if (minBright > 0)
        new_brightness = 0xff & (new_brightness * (0x100 - minBright) / 0x100) + minBright;

    pthread_mutex_lock(&g_lock);
    if (new_brightness != lastHID){
        if (hid_fd < 0) openhid();
        if (hid_fd >= 0) writehid(0x03, new_brightness, 2);
        lastHID = new_brightness;
    }
    pthread_mutex_unlock(&g_lock);
    return 0;
}

static int close_lights(struct light_device_t *dev){
    if (dev) {
        free(dev);
    }
    return 0;
}

/** Open a new instance of a lights device using name */
static int open_lights(const struct hw_module_t* module, char const* name,
        struct hw_device_t** device){
    int (*set_light)(struct light_device_t* dev,
            struct light_state_t const* state);

    if (0 == strcmp(LIGHT_ID_BACKLIGHT, name))
        set_light = set_light_backlight;
    else
        return -EINVAL;

    pthread_once(&g_init, init_globals);

    struct light_device_t *dev = malloc(sizeof(struct light_device_t));
    memset(dev, 0, sizeof(*dev));

    char mlight[PROP_VALUE_MAX];
    if (property_get("persist.backlight.minimum", mlight, "") > 0){
        __android_log_print(ANDROID_LOG_DEBUG, "lights", "setting minimum brightness: %02X", minBright);
        minBright = (uint8_t) (atoi(mlight) & 0xff);
    }

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t*)module;
    dev->common.close = (int (*)(struct hw_device_t*))close_lights;
    dev->set_light = set_light;

    *device = (struct hw_device_t*)dev;
    return 0;
}

static struct hw_module_methods_t lights_module_methods = {
    .open =  open_lights,
};

struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = LIGHTS_HARDWARE_MODULE_ID,
    .name = "lights Module",
    .author = "Adam Serbinski",
    .methods = &lights_module_methods,
};
