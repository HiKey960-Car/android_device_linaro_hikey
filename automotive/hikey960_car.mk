$(call inherit-product, device/linaro/hikey/hikey960.mk)
PRODUCT_PACKAGE_OVERLAYS += device/linaro/hikey/automotive/overlay
$(call inherit-product, device/generic/car/common/car.mk)

PRODUCT_COPY_FILES += device/linaro/hikey/automotive/manifest.xml:$(TARGET_COPY_OUT_VENDOR)/etc/vintf/manifest/manifest-car.xml

# Automotive Audio HAL
include device/linaro/hikey/automotive/audio/car_audio.mk

# ADB over IP
PRODUCT_PROPERTY_OVERRIDES += service.adb.tcp.port=5555

# ueventd.hikey960.rc
PRODUCT_COPY_FILES += \
    device/linaro/hikey/automotive/ueventd.hikey960.rc:$(TARGET_COPY_OUT_ROOT)/ueventd.hikey960.rc

# Broadcast Radio HAL
PRODUCT_PACKAGES += android.hardware.broadcastradio@1.1-service.tef6686
PRODUCT_COPY_FILES += \
    device/linaro/hikey/automotive/broadcastradio/android.hardware.broadcastradio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.broadcastradio.xml

# Audio debugging packages
PRODUCT_PACKAGES += \
    tinymix \
    tinypcminfo \
    tinyhostless \
    tinycap

# Custom
PRODUCT_PACKAGES += CarConfig \
    thermalfand \
    camd \
    bossac

# Lights
PRODUCT_PACKAGES += \
    lights.hikey960 \
    android.hardware.light@2.0 \
    android.hardware.light@2.0-service \
    android.hardware.light@2.0-impl
PRODUCT_COPY_FILES += \
    device/linaro/hikey/automotive/liblight/android.hardware.light@2.0-service.xml:$(TARGET_COPY_OUT_VENDOR)/etc/vintf/manifest/android.hardware.light@2.0-service.xml

BOARD_SEPOLICY_DIRS += device/linaro/hikey/automotive/sepolicy

# GNSS HAL - GPSD
PRODUCT_PACKAGES += android.hardware.gnss@1.1 \
        android.hardware.gnss@1.1-impl \
        android.hardware.gnss@1.1-service.gpsd \
        gpsd
PRODUCT_PROPERTY_OVERRIDES += \
        service.gpsd.automotive=true \
        service.gpsd.parameters=-N,-D2,/dev/ttyACM0,/dev/ttyACM1
BOARD_SEPOLICY_DIRS += external/gpsd/android/sepolicy
DEVICE_MANIFEST_FILE += external/gpsd/android/manifest.xml

# Prebuilt
include device/linaro/hikey/automotive/prebuilt/prebuilt.mk

PRODUCT_NAME := hikey960_car
PRODUCT_DEVICE := hikey960
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP CAR on hikey960
