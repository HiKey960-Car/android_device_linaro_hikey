package tk.rabidbeaver.carsettings;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.content.SharedPreferences;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;

import java.util.HashSet;
import java.util.Set;

public class CarSettings extends Activity {
    Spinner spin;
    TextView adcout1, adcout2;
    Thread th;
    boolean adcrunner;
    boolean success;

    static {
        System.loadLibrary("jni_swi");
    }
    public native int open();
    public native int close();
    public native int send(int cmd, int value);
    public native int getswi();

    private boolean setContainsString(Set<String> set, String string){
        Object[] setarr = set.toArray();
        for (int i=0; i<set.size(); i++){
            if (((String)setarr[i]).contentEquals(string)) return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_swi);

        SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
        Set<String> selectedDevices = prefs.getStringSet("devices", new HashSet<String>());

        boolean autoconnect = prefs.getBoolean("autoconnect", false);
        boolean autohotspot = prefs.getBoolean("autohotspot", false);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        PairedDev[] pda = new PairedDev[pairedDevices.size()];
        boolean[] selected = new boolean[pairedDevices.size()];

        int i = 0;
        for (BluetoothDevice device : pairedDevices) {
            Log.d("BluetoothTethering",device.getName()+", "+device.getAddress());
            if (selectedDevices.size() > 0 && setContainsString(selectedDevices, device.getAddress())) selected[i] = true;
            pda[i] = new PairedDev(device);
            i++;
        }

        final MultiSpinner spinner = (MultiSpinner) findViewById(R.id.devspin);
        final ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, pda);
        spinner.setAdapter(spinnerArrayAdapter, false, null);

        spinner.setSelected(selected);

        Switch panswitch = findViewById(R.id.autopan);
        panswitch.setChecked(autoconnect);
        panswitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
                editor.putBoolean("autoconnect", isChecked);
                Set<String> selectedItems = new HashSet<>();
                for (int i=0; i<spinnerArrayAdapter.getCount(); i++){
                    if (spinner.getSelected()[i]) selectedItems.add(((PairedDev)spinnerArrayAdapter.getItem(i)).getDev());
                }
                editor.putStringSet("devices", selectedItems);
                editor.apply();
            }
        });

        Switch hotspotswitch = findViewById(R.id.onhotspot);
        hotspotswitch.setChecked(autohotspot);
        hotspotswitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
                editor.putBoolean("autohotspot", isChecked);
                editor.apply();
            }
        });

        adcout1 = findViewById(R.id.adcout1);
        adcout2 = findViewById(R.id.adcout2);

        Button serdbg = findViewById(R.id.serdbgbtn);
        serdbg.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                send(0x10, 0);
            }
        });

        Button startbtn = findViewById(R.id.startbtn);
        startbtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                open();
                try {
                    Thread.sleep(500);
                } catch (Exception e){
                    e.printStackTrace();
                }
                send(0x30, 0);
		adcrunner = true;
                th = new Thread(new Runnable(){
                    @Override
                    public void run(){
                        while (adcrunner){
                            try {
                                Thread.sleep(500);
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable(){
                                @Override
                                public void run(){
                                    int swival = getswi();
                                    adcout1.setText(Integer.toString(swival & 0xffff));
                                    adcout2.setText(Integer.toString((swival >> 16) & 0xffff));
                                }
                            });
                        }
                    }
                });
                th.start();
            }
        });

        Button savebtn = findViewById(R.id.savebtn);
        savebtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                int value = ((CodeValue) spin.getSelectedItem()).code;
                Log.d("SWI", "Send returned: " + send(0x40, value));
            }
        });

        Button stopbtn = findViewById(R.id.endbtn);
        stopbtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                adcrunner = false;
                th.interrupt();
                Log.d("SWI", "Send returned: " + send(0x50, 0));
                try {
                    Thread.sleep(500);
                } catch (Exception e){
                    e.printStackTrace();
                }
                Log.d("SWI", "Close returned: " + close());
            }
        });

        Button cancelbtn = findViewById(R.id.cancelbtn);
        cancelbtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                adcrunner = false;
                th.interrupt();
                Log.d("SWI", "Send returned: " + send(0x60, 0));
                try {
                    Thread.sleep(500);
                } catch (Exception e){
                    e.printStackTrace();
                }
                Log.d("SWI", "Close returned: " + close());
            }
        });

        Button reconnect = findViewById(R.id.reconnect);
        reconnect.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                if (close() == 1) open();
            }
        });

        Button upgradeBtn = findViewById(R.id.progbtn);
        upgradeBtn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View btn){
                TextView upgmsg = findViewById(R.id.upgmsg);
                upgmsg.setText("Firmware upgrade in progress. Please wait.");

               // open();
               // send(0xFF, 0);

                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select firmware"),123);
            }
        });

        spin = findViewById(R.id.hidcodes);
        List<CodeValue> mCodeValues = genlist();
        ArrayAdapter codeAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_item, mCodeValues);
        spin.setAdapter(codeAdapter);

        Log.d("CarSettings", "Starting CarLocationService");
        Intent startServiceIntent = new Intent(this, CarLocationService.class);
        startService(startServiceIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123 && resultCode == RESULT_OK) {
            Uri selectedfile = data.getData();
            if (!selectedfile.toString().startsWith("content://com.android.providers.downloads.documents/document/raw%3A")){
                TextView upgmsg = findViewById(R.id.upgmsg);
                upgmsg.setText("Invalid file");

                return;
            }
            final String fwpath = selectedfile.toString().substring(67).replace("%2F", "/");
            Log.d("SWIFIRMWARE", fwpath);

            new Thread(new Runnable(){
                @Override
                public void run(){
                    open();
                    try {
                        Thread.sleep(500);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    send(0xFF, 0);
                    try {
                        Thread.sleep(500);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    close();

                    // If the machine has rebooted after the mcu was wiped, then gpsd could interfere with bossac's access to the
                    // serial port. Stop gnss-gpsd before attempting to write the MCU.
                    String[] cmd1 = { "sh", "-c", "/system/bin/stop gnss-gpsd" };
                    try {
                        Process p = Runtime.getRuntime().exec(cmd1);
                        p.waitFor();
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                    success = false;
                    for (int i = 0; i < 5 && !success; i++){
                        String[] cmdline = { "sh", "-c", "/vendor/bin/logwrapper /vendor/bin/bossac -i -d --port=/dev/ttyACM"
                                + Integer.toString(i) + " -U -i --offset=0x2000 -e -w -v " + fwpath + " -R" };

                        try {
                            if (i == 0) Thread.sleep(1000);
                            Process p = Runtime.getRuntime().exec(cmdline);
                            p.waitFor();
                            if (p.exitValue() == 0) success = true;
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    runOnUiThread(new Runnable(){
                        @Override
                        public void run(){
                            TextView upgmsg = findViewById(R.id.upgmsg);
                            if (success) upgmsg.setText("Firmware upgrade SUCCESS.");
                            else upgmsg.setText("Firmware upgrade FAILURE.");
                        }
                    });
                }
            }).start();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        adcrunner = false;
        try {
		th.interrupt();
	} catch (Exception e){e.printStackTrace();}
	if (send(0x60, 0) == 1) close();
    }

    private class PairedDev {
        String dev;
        String name;
        PairedDev(BluetoothDevice btd){
            name = btd.getName();
            dev = btd.getAddress();
        }
        public String toString(){return name;}
        String getDev(){return dev;}
    }

    private List<CodeValue> genlist(){
        List<CodeValue> mCodeValues = new ArrayList<CodeValue>();
        mCodeValues.add(new CodeValue(0x030, "KEY_POWER"));
        mCodeValues.add(new CodeValue(0x031, "KEY_RESTART"));
        mCodeValues.add(new CodeValue(0x032, "KEY_SLEEP"));
        mCodeValues.add(new CodeValue(0x034, "KEY_SLEEP"));
        mCodeValues.add(new CodeValue(0x035, "KEY_KBDILLUMTOGGLE"));
        mCodeValues.add(new CodeValue(0x036, "BTN_MISC"));
        mCodeValues.add(new CodeValue(0x040, "KEY_MENU"));
        mCodeValues.add(new CodeValue(0x041, "KEY_SELECT"));
        mCodeValues.add(new CodeValue(0x042, "KEY_UP"));
        mCodeValues.add(new CodeValue(0x043, "KEY_DOWN"));
        mCodeValues.add(new CodeValue(0x044, "KEY_LEFT"));
        mCodeValues.add(new CodeValue(0x045, "KEY_RIGHT"));
        mCodeValues.add(new CodeValue(0x046, "KEY_ESC"));
        mCodeValues.add(new CodeValue(0x047, "KEY_KPPLUS"));
        mCodeValues.add(new CodeValue(0x048, "KEY_KPMINUS"));
        mCodeValues.add(new CodeValue(0x060, "KEY_INFO"));
        mCodeValues.add(new CodeValue(0x061, "KEY_SUBTITLE"));
        mCodeValues.add(new CodeValue(0x063, "KEY_VCR"));
        mCodeValues.add(new CodeValue(0x065, "KEY_CAMERA"));
        mCodeValues.add(new CodeValue(0x069, "KEY_RED"));
        mCodeValues.add(new CodeValue(0x06a, "KEY_GREEN"));
        mCodeValues.add(new CodeValue(0x06b, "KEY_BLUE"));
        mCodeValues.add(new CodeValue(0x06c, "KEY_YELLOW"));
        mCodeValues.add(new CodeValue(0x06d, "KEY_ASPECT_RATIO"));
        mCodeValues.add(new CodeValue(0x06f, "KEY_BRIGHTNESSUP"));
        mCodeValues.add(new CodeValue(0x070, "KEY_BRIGHTNESSDOWN"));
        mCodeValues.add(new CodeValue(0x072, "KEY_BRIGHTNESS_TOGGLE"));
        mCodeValues.add(new CodeValue(0x073, "KEY_BRIGHTNESS_MIN"));
        mCodeValues.add(new CodeValue(0x074, "KEY_BRIGHTNESS_MAX"));
        mCodeValues.add(new CodeValue(0x075, "KEY_BRIGHTNESS_AUTO"));
        mCodeValues.add(new CodeValue(0x079, "KEY_KBDILLUMUP"));
        mCodeValues.add(new CodeValue(0x07a, "KEY_KBDILLUMDOWN"));
        mCodeValues.add(new CodeValue(0x07c, "KEY_KBDILLUMTOGGLE"));
        mCodeValues.add(new CodeValue(0x082, "KEY_VIDEO_NEXT"));
        mCodeValues.add(new CodeValue(0x083, "KEY_LAST"));
        mCodeValues.add(new CodeValue(0x084, "KEY_ENTER"));
        mCodeValues.add(new CodeValue(0x088, "KEY_PC"));
        mCodeValues.add(new CodeValue(0x089, "KEY_TV"));
        mCodeValues.add(new CodeValue(0x08a, "KEY_WWW"));
        mCodeValues.add(new CodeValue(0x08b, "KEY_DVD"));
        mCodeValues.add(new CodeValue(0x08c, "KEY_PHONE"));
        mCodeValues.add(new CodeValue(0x08d, "KEY_PROGRAM"));
        mCodeValues.add(new CodeValue(0x08e, "KEY_VIDEOPHONE"));
        mCodeValues.add(new CodeValue(0x08f, "KEY_GAMES"));
        mCodeValues.add(new CodeValue(0x090, "KEY_MEMO"));
        mCodeValues.add(new CodeValue(0x091, "KEY_CD"));
        mCodeValues.add(new CodeValue(0x092, "KEY_VCR"));
        mCodeValues.add(new CodeValue(0x093, "KEY_TUNER"));
        mCodeValues.add(new CodeValue(0x094, "KEY_EXIT"));
        mCodeValues.add(new CodeValue(0x095, "KEY_HELP"));
        mCodeValues.add(new CodeValue(0x096, "KEY_TAPE"));
        mCodeValues.add(new CodeValue(0x097, "KEY_TV2"));
        mCodeValues.add(new CodeValue(0x098, "KEY_SAT"));
        mCodeValues.add(new CodeValue(0x09a, "KEY_PVR"));
        mCodeValues.add(new CodeValue(0x09c, "KEY_CHANNELUP"));
        mCodeValues.add(new CodeValue(0x09d, "KEY_CHANNELDOWN"));
        mCodeValues.add(new CodeValue(0x0a0, "KEY_VCR2"));
        mCodeValues.add(new CodeValue(0x0b0, "KEY_PLAY"));
        mCodeValues.add(new CodeValue(0x0b1, "KEY_PAUSE"));
        mCodeValues.add(new CodeValue(0x0b2, "KEY_RECORD"));
        mCodeValues.add(new CodeValue(0x0b3, "KEY_FASTFORWARD"));
        mCodeValues.add(new CodeValue(0x0b4, "KEY_REWIND"));
        mCodeValues.add(new CodeValue(0x0b5, "KEY_NEXTSONG"));
        mCodeValues.add(new CodeValue(0x0b6, "KEY_PREVIOUSSONG"));
        mCodeValues.add(new CodeValue(0x0b7, "KEY_STOPCD"));
        mCodeValues.add(new CodeValue(0x0b8, "KEY_EJECTCD"));
        mCodeValues.add(new CodeValue(0x0bc, "KEY_MEDIA_REPEAT"));
        mCodeValues.add(new CodeValue(0x0b9, "KEY_SHUFFLE"));
        mCodeValues.add(new CodeValue(0x0bf, "KEY_SLOW"));
        mCodeValues.add(new CodeValue(0x0cd, "KEY_PLAYPAUSE"));
        mCodeValues.add(new CodeValue(0x0cf, "KEY_VOICECOMMAND"));
        mCodeValues.add(new CodeValue(0x0e0, "ABS_VOLUME"));
        mCodeValues.add(new CodeValue(0x0e2, "KEY_MUTE"));
        mCodeValues.add(new CodeValue(0x0e5, "KEY_BASSBOOST"));
        mCodeValues.add(new CodeValue(0x0e9, "KEY_VOLUMEUP"));
        mCodeValues.add(new CodeValue(0x0ea, "KEY_VOLUMEDOWN"));
        mCodeValues.add(new CodeValue(0x0f5, "KEY_SLOW"));
        mCodeValues.add(new CodeValue(0x181, "KEY_BUTTONCONFIG"));
        mCodeValues.add(new CodeValue(0x182, "KEY_BOOKMARKS"));
        mCodeValues.add(new CodeValue(0x183, "KEY_CONFIG"));
        mCodeValues.add(new CodeValue(0x184, "KEY_WORDPROCESSOR"));
        mCodeValues.add(new CodeValue(0x185, "KEY_EDITOR"));
        mCodeValues.add(new CodeValue(0x186, "KEY_SPREADSHEET"));
        mCodeValues.add(new CodeValue(0x187, "KEY_GRAPHICSEDITOR"));
        mCodeValues.add(new CodeValue(0x188, "KEY_PRESENTATION"));
        mCodeValues.add(new CodeValue(0x189, "KEY_DATABASE"));
        mCodeValues.add(new CodeValue(0x18a, "KEY_MAIL"));
        mCodeValues.add(new CodeValue(0x18b, "KEY_NEWS"));
        mCodeValues.add(new CodeValue(0x18c, "KEY_VOICEMAIL"));
        mCodeValues.add(new CodeValue(0x18d, "KEY_ADDRESSBOOK"));
        mCodeValues.add(new CodeValue(0x18e, "KEY_CALENDAR"));
        mCodeValues.add(new CodeValue(0x18f, "KEY_TASKMANAGER"));
        mCodeValues.add(new CodeValue(0x190, "KEY_JOURNAL"));
        mCodeValues.add(new CodeValue(0x191, "KEY_FINANCE"));
        mCodeValues.add(new CodeValue(0x192, "KEY_CALC"));
        mCodeValues.add(new CodeValue(0x193, "KEY_PLAYER"));
        mCodeValues.add(new CodeValue(0x194, "KEY_FILE"));
        mCodeValues.add(new CodeValue(0x196, "KEY_WWW"));
        mCodeValues.add(new CodeValue(0x199, "KEY_CHAT"));
        mCodeValues.add(new CodeValue(0x19c, "KEY_LOGOFF"));
        mCodeValues.add(new CodeValue(0x19e, "KEY_COFFEE"));
        mCodeValues.add(new CodeValue(0x19f, "KEY_CONTROLPANEL"));
        mCodeValues.add(new CodeValue(0x1a2, "KEY_APPSELECT"));
        mCodeValues.add(new CodeValue(0x1a3, "KEY_NEXT"));
        mCodeValues.add(new CodeValue(0x1a4, "KEY_PREVIOUS"));
        mCodeValues.add(new CodeValue(0x1a6, "KEY_HELP"));
        mCodeValues.add(new CodeValue(0x1a7, "KEY_DOCUMENTS"));
        mCodeValues.add(new CodeValue(0x1ab, "KEY_SPELLCHECK"));
        mCodeValues.add(new CodeValue(0x1ae, "KEY_KEYBOARD"));
        mCodeValues.add(new CodeValue(0x1b1, "KEY_SCREENSAVER"));
        mCodeValues.add(new CodeValue(0x1b4, "KEY_FILE"));
        mCodeValues.add(new CodeValue(0x1b6, "KEY_IMAGES"));
        mCodeValues.add(new CodeValue(0x1b7, "KEY_AUDIO"));
        mCodeValues.add(new CodeValue(0x1b8, "KEY_VIDEO"));
        mCodeValues.add(new CodeValue(0x1bc, "KEY_MESSENGER"));
        mCodeValues.add(new CodeValue(0x1bd, "KEY_INFO"));
        mCodeValues.add(new CodeValue(0x1cb, "KEY_ASSISTANT"));
        mCodeValues.add(new CodeValue(0x201, "KEY_NEW"));
        mCodeValues.add(new CodeValue(0x202, "KEY_OPEN"));
        mCodeValues.add(new CodeValue(0x203, "KEY_CLOSE"));
        mCodeValues.add(new CodeValue(0x204, "KEY_EXIT"));
        mCodeValues.add(new CodeValue(0x207, "KEY_SAVE"));
        mCodeValues.add(new CodeValue(0x208, "KEY_PRINT"));
        mCodeValues.add(new CodeValue(0x209, "KEY_PROPS"));
        mCodeValues.add(new CodeValue(0x21a, "KEY_UNDO"));
        mCodeValues.add(new CodeValue(0x21b, "KEY_COPY"));
        mCodeValues.add(new CodeValue(0x21c, "KEY_CUT"));
        mCodeValues.add(new CodeValue(0x21d, "KEY_PASTE"));
        mCodeValues.add(new CodeValue(0x21f, "KEY_FIND"));
        mCodeValues.add(new CodeValue(0x221, "KEY_SEARCH"));
        mCodeValues.add(new CodeValue(0x222, "KEY_GOTO"));
        mCodeValues.add(new CodeValue(0x223, "KEY_HOMEPAGE"));
        mCodeValues.add(new CodeValue(0x224, "KEY_BACK"));
        mCodeValues.add(new CodeValue(0x225, "KEY_FORWARD"));
        mCodeValues.add(new CodeValue(0x226, "KEY_STOP"));
        mCodeValues.add(new CodeValue(0x227, "KEY_REFRESH"));
        mCodeValues.add(new CodeValue(0x22a, "KEY_BOOKMARKS"));
        mCodeValues.add(new CodeValue(0x22d, "KEY_ZOOMIN"));
        mCodeValues.add(new CodeValue(0x22e, "KEY_ZOOMOUT"));
        mCodeValues.add(new CodeValue(0x22f, "KEY_ZOOMRESET"));
        mCodeValues.add(new CodeValue(0x232, "KEY_FULL_SCREEN"));
        mCodeValues.add(new CodeValue(0x233, "KEY_SCROLLUP"));
        mCodeValues.add(new CodeValue(0x234, "KEY_SCROLLDOWN"));
        mCodeValues.add(new CodeValue(0x238, "KEY_AC_PAN"));
        mCodeValues.add(new CodeValue(0x23d, "KEY_EDIT"));
        mCodeValues.add(new CodeValue(0x25f, "KEY_CANCEL"));
        mCodeValues.add(new CodeValue(0x269, "KEY_INSERT"));
        mCodeValues.add(new CodeValue(0x26a, "KEY_DELETE"));
        mCodeValues.add(new CodeValue(0x279, "KEY_REDO"));
        mCodeValues.add(new CodeValue(0x289, "KEY_REPLY"));
        mCodeValues.add(new CodeValue(0x28b, "KEY_FORWARDMAIL"));
        mCodeValues.add(new CodeValue(0x28c, "KEY_SEND"));
        mCodeValues.add(new CodeValue(0x29d, "KEY_KBD_LAYOUT_NEXT"));
        mCodeValues.add(new CodeValue(0x2c7, "KEY_KBDINPUTASSIST_PREV"));
        mCodeValues.add(new CodeValue(0x2c8, "KEY_KBDINPUTASSIST_NEXT"));
        mCodeValues.add(new CodeValue(0x2c9, "KEY_KBDINPUTASSIST_PREVGROUP"));
        mCodeValues.add(new CodeValue(0x2ca, "KEY_KBDINPUTASSIST_NEXTGROUP"));
        mCodeValues.add(new CodeValue(0x2cb, "KEY_KBDINPUTASSIST_ACCEPT"));
        mCodeValues.add(new CodeValue(0x2cc, "KEY_KBDINPUTASSIST_CANCEL"));
        mCodeValues.add(new CodeValue(0x29f, "KEY_SCALE"));
        mCodeValues.add(new CodeValue(0xffff, "__RESET SBC__"));
        return mCodeValues;
    }

    private class CodeValue {
        int code;
        String name;

        CodeValue(int mCode, String mName){
            code = mCode;
            name = "0x" + Integer.toHexString(mCode) + ": " + mName;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
}

