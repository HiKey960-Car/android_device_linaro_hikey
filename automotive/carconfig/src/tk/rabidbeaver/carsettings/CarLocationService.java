package tk.rabidbeaver.carsettings;

import android.Manifest;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class CarLocationService extends Service {
    LocationManager locationManager;
    double longitudeGPS, latitudeGPS;
    Notification.Builder mNotificationBuilder;

    public CarLocationService(){}

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not for binding");
    }

    @Override
    public void onCreate(){
        super.onCreate();
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (getPackageManager().checkPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                getPackageName()) == PackageManager.PERMISSION_GRANTED) {
            Log.d("CarLocationService", "Permission granted, registering for location");
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5 * 1000, 0, locationListenerGPS);
        }

        mNotificationBuilder = new Notification.Builder(this)
                .setContentTitle("Car Location Service")
                .setContentText("GPS KeepAlive")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setOnlyAlertOnce(true)
                .setOngoing(true);
        startForeground(131313, mNotificationBuilder.build());

        return START_STICKY;
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();
            mNotificationBuilder.setContentText("Latitude: " + latitudeGPS + ", Longitude: " + longitudeGPS);
            startForeground(131313, mNotificationBuilder.build());
            Log.d("CarLocationService", "Latitude: " + latitudeGPS + ", Longitude: " + longitudeGPS);
        }

        public void onStatusChanged(String s, int i, Bundle bundle) {}
        public void onProviderEnabled(String s) {}
        public void onProviderDisabled(String s) {}
    };
}

