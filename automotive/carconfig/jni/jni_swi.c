#include "jni.h"
#include <android/log.h>

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include <fcntl.h>
#include <sys/types.h>

#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <dirent.h>

static int hid_fd = -1;
static pthread_t runner_thread;
static pthread_mutex_t hid_lock;
static int threadrun;

typedef struct swi_context {
    JavaVM *javaVM;
    jclass   mainActivityClz;
    jobject  mainActivityObj;
    uint32_t swi;
    uint8_t ack;
} SWIContext;
SWIContext g_ctx;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
    memset(&g_ctx, 0, sizeof(g_ctx));
    g_ctx.javaVM = vm;
    g_ctx.mainActivityObj = NULL;
    pthread_mutex_init(&hid_lock, NULL);
    g_ctx.ack = 0;
    return  JNI_VERSION_1_6;
}

int writehid(uint8_t cmd, uint8_t b1, uint8_t b2, uint8_t len){
    uint8_t buf[16];
    int res;

    buf[0] = 0x0;   // Report ID (0)
    buf[1] = cmd;   // Command (0x20 - query, 0x02 - set)
    buf[2] = b1;    // First byte value to set
    buf[3] = b2;    // Second byte value to set

    __android_log_print(ANDROID_LOG_DEBUG, "swi", "Writing buf[4]: 0x %02X %02X %02X %02X", buf[0], buf[1], buf[2], buf[3]);

    res = write(hid_fd, buf, len+1);
    if (res < 0){
        __android_log_print(ANDROID_LOG_DEBUG, "swi", "HID Write error: %d", errno);
        if (errno != 75 && errno != 71 && errno != 32){
            close(hid_fd);
            hid_fd = -1;
        }
    }
    return res;
}

int hidrawdev(const struct dirent *d){
    if (strstr(d->d_name, "hidraw") != NULL) return 1;
    return 0;
}

void *runner(void* context){
    uint8_t buf[16];
    int res;

    SWIContext *pctx = (SWIContext *) context;

    while (threadrun){
        if (hid_fd >= 0){
            res = read(hid_fd, buf, 16);
            if (res == 5){
                 pctx->swi = buf[1]*16777216 + buf[2]*65536 + buf[3]*256 + buf[4];
                 __android_log_print(ANDROID_LOG_DEBUG, "swi",
                       "Bytes from SWI: 0x%02X %02X%02X %02X%02X",
                       buf[0], buf[1], buf[2], buf[3], buf[4]);
            } else if (res == 2 && buf[0] == 0x40 && buf[1] == 0x40){
		__android_log_print(ANDROID_LOG_DEBUG, "swi", "Received ACK");
                pthread_mutex_lock(&hid_lock);
                pctx->ack = 1;
                pthread_mutex_unlock(&hid_lock);
            }
        } else
            usleep(10000);
    }

    return 0;
}

JNIEXPORT JNICALL int Java_tk_rabidbeaver_carsettings_CarSettings_getswi (JNIEnv *env, jobject instance){
    return g_ctx.swi;
}

JNIEXPORT JNICALL int Java_tk_rabidbeaver_carsettings_CarSettings_open (JNIEnv *env, jobject instance){
    int n, res;
    struct dirent **filelist;
    char buf[256];
    char path[16];
    struct hidraw_report_descriptor rpt_desc;
    struct hidraw_devinfo info;

    if (hid_fd < 0) {
        n = scandir("/dev", &filelist, *hidrawdev, NULL);
        __android_log_print(ANDROID_LOG_DEBUG, "swi", "Found %d hidraw devices.", n);
        while (n--){
            snprintf(path, sizeof(path), "/dev/%s", filelist[n]->d_name);
            free(filelist[n]);
            __android_log_print(ANDROID_LOG_DEBUG, "swi", "File: %s", path);

            if (hid_fd < 0){
                hid_fd = open(path, O_RDWR|O_NONBLOCK);
                if (hid_fd < 0){
                    __android_log_print(ANDROID_LOG_DEBUG, "swi", "hid_fd < 0 -- failed to open file.");
                    continue;
                }
                __android_log_print(ANDROID_LOG_DEBUG, "swi", "file opened");

                memset(&rpt_desc, 0x0, sizeof(rpt_desc));
                memset(&info, 0x0, sizeof(info));
                memset(buf, 0x0, sizeof(buf));

                /* Get Raw Info */
                res = ioctl(hid_fd, HIDIOCGRAWINFO, &info);

                if ((int16_t)info.vendor != (int16_t)0x239a){
                    __android_log_print(ANDROID_LOG_DEBUG, "swi", "wrong vendor");
                    close(hid_fd);
                    hid_fd = -1;
                    continue;
                }

                if ((int16_t)info.product != (int16_t)0x801e){
                    __android_log_print(ANDROID_LOG_DEBUG, "swi", "wrong product");
                    close(hid_fd);
                    hid_fd = -1;
                    continue;
                }
                __android_log_print(ANDROID_LOG_DEBUG, "swi", "device OK, checking endpoint");

                // Command 0x20 requests which type, 0x01 = swi
                writehid(0x20, 0x00, 0x00, 1);

                // Try to read data from this endpoint
                for (int i=0; i<10; i++){
                    __android_log_print(ANDROID_LOG_DEBUG, "swi", "waiting for response: %d / 10", i);
                    if ((res = read(hid_fd, buf, 16)) > 0){
                        __android_log_print(ANDROID_LOG_DEBUG, "swi", "received data: %02X\n", buf[0]);
                        if (buf[0] == 0x02){
                            // re-open the fd in blocking mode.
                            __android_log_print(ANDROID_LOG_DEBUG, "swi", "Found correct endpoint, reconnecting in blocking mode");
                            close(hid_fd);
                            hid_fd = open(path, O_RDWR);
                            __android_log_print(ANDROID_LOG_DEBUG, "swi", "New FD: %d", hid_fd);
                            threadrun = 1;
                            pthread_create(&runner_thread, NULL, runner, &g_ctx);
                        } else {
                            __android_log_print(ANDROID_LOG_DEBUG, "swi", "Wrong endpoint.");
                            close(hid_fd);
                            hid_fd = -1;
                        }
                        break;
                    }
                    usleep(10000);
                }
                if (threadrun == 0 && hid_fd >= 0){
                    close(hid_fd);
                    hid_fd = -1;
                }
            }
        }
        free(filelist);
    }
    if (hid_fd >= 0) return 1;
    return 0;
}

JNIEXPORT JNICALL int Java_tk_rabidbeaver_carsettings_CarSettings_close (JNIEnv *env, jobject instance){
    threadrun = 0;
    __android_log_print(ANDROID_LOG_DEBUG, "swi", "Closing, fd = %d", hid_fd);
    if (hid_fd >= 0) close(hid_fd);
    hid_fd = -1;
    return 1;
}

JNIEXPORT JNICALL int Java_tk_rabidbeaver_carsettings_CarSettings_send (JNIEnv *env, jobject instance, jint cmd, jint value){
    int res = -1;
    int i, j;
    int success = 0;
    __android_log_print(ANDROID_LOG_DEBUG, "swi", "Sending, fd = %d", hid_fd);
    if (hid_fd < 0) return 0;

    pthread_mutex_lock(&hid_lock);
    g_ctx.ack = 0;
    pthread_mutex_unlock(&hid_lock);

    for (i=0; i<5 && !success; i++){
        if (value == 0) res = writehid((uint8_t) cmd, 0x00, 0x00, 1);
        else {
            res = writehid((uint8_t) cmd, (uint8_t)(value >> 8 & 0xff), (uint8_t)(value & 0xff), 3);
        }

        for (j=0; j<10 && !success; j++){
            pthread_mutex_lock(&hid_lock);
            if (g_ctx.ack == 1) success = 1;
            pthread_mutex_unlock(&hid_lock);

            if (success != 1) usleep(10000);
        }
    }

    if (res >= 0 && success) return 1;
    return 0;
}
