#include "tef6686.h"

/*====================================================
 Function: tune
 Input: 
      up
 OutPut:
      Null
 Desp:
       manual set tuner sub
=========================================================*/
void tune(int up){
	/*change one step of tuning frequency*/
	Radio_ChangeFreqOneStep(up);

	/*write "new programmble value" into PLL*/
	Radio_SetFreq(Radio_PRESETMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());
	/*clear station number*/
	Radio_ClearCurrentStation();
}

void tune_direct(uint16_t freq, int fm){
	Radio_SetFreq(Radio_PRESETMODE, fm ? FM1_BAND : MW_BAND, freq);
	Radio_ClearCurrentStation();
}

/*====================================================
 Function: seek
 Input: 
      up
 OutPut:
      Null
 Desp:
       auto seek
=========================================================*/
int seek(int up){
//	printf("seek\n");
	/*to process seek event step by step*/

	int terminate = 0;
	int mode = 20;
	int stepcount = 0;
	int startfreq = Radio_GetCurrentFreq();

	while (!terminate){
	switch(mode){
		case 20:/*Loop start*/
		        stepcount++;
//			printf("Running 20\n");
			/*increase or decreas one step*/
			Radio_ChangeFreqOneStep(up);

			/*write "new programmble value" into PLL*/
			Radio_SetFreq(Radio_SEARCHMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());
			
			mode = 30;//set tunersubmode
			Radio_CheckStationInit();//set check SD and IF start step
			Radio_ClearCurrentStation();//clear station number
			break;
			
		/*check Signel level and IF step after wait timer */
		case 30:
			/*station check*/
//			printf("Running 30\n");
			usleep(20000);
			Radio_CheckStation();   
			if(Radio_CheckStationStatus() >= NO_STATION)
				mode = 40; 
			break;

		case 40:
//			printf("Running 40\n");
			if(Radio_CheckStationStatus()== NO_STATION)  {/*no station*/			 	
				if(startfreq==Radio_GetCurrentFreq())  
					mode = 50;	
				else 
					mode = 20;
			}
			else if(Radio_CheckStationStatus()== PRESENT_STATION){
				mode = 50;
			}
			break;
			
		case 50:/*stop Seek Tuning*/
			/*Tuner finish current work mode*/
//			printf("Running 50\n");
			Radio_SetFreq(Radio_PRESETMODE, Radio_GetCurrentBand(), Radio_GetCurrentFreq());
			terminate = 1;
			return 1;
	}
	}
	return 0;
}

/*====================================================
 Function:Tuner_BandChange_Sub
 Input: 
      Null
 OutPut:
      Null
 Desp:
      band key action
=========================================================*/
void Tuner_BandChange_Sub(void)
{
	/*set to next band*/
	Radio_NextBand();
	/*rewrite parameters*/
}

/*====================================================
 Function:Tuner_Check_Stereo
 Input: 
      Null
 OutPut:
      Null
 Desp:
      check fm stereo signal
=========================================================*/
int Tuner_Check_Stereo(void){
	return Radio_CheckStereo();
}

void Tuner_SetSeekSens_Sub(int high){
	Radio_SetSeekSenLevel(high);
}

//return -1 if device doesn't exist
//return  0 if device busy
//return  1 if device idle or boot state
//return  2 if device active
int Tuner_Power_on(void)
{
	static int PowerOn_Counter=0;
	TUNER_STATE status;
	int r;

	{
//Wait until device is found present on the I2C bus boot state
//Repeat APPL_Get_Operation_Status read until I2C acknowledge from device;
//status = 0 boot state is found.
//[ w 40 80 01 [ r 0000 ]
		if (1 == APPL_Get_Operation_Status(&status)){
			PowerOn_Counter = 0;
			r = 1;
			if (status == eTuner_Active_state) r = 2;
		} else if(++PowerOn_Counter > 50){
			PowerOn_Counter = 0;
			r = -1;	//return with not exist
		} else {
			r = 0;	//return with busy
		}
	}

	return r;
}

